import axios from 'axios'

export default {
    namespaced:true,
    state:{
       allposts:[]
    },
    getters:{
        reponsePosts(state){
            return state.allposts
        },
    },
    mutations:{
        SET_ALL_POSTS(state,value){
            state.allposts = value
        },
    },
    actions:{
        async getAllPost({dispatch}){
            console.log('solicitando todas las publicaciones')
            await axios.get('/sanctum/csrf-cookie')
            let gp = await axios.get('/api/posts')
            return dispatch('mePosts',gp.data)
        },
        mePosts({commit},getAllPost){
            console.log(getAllPost)
            commit('SET_ALL_POSTS',getAllPost)
        }
    }
}
